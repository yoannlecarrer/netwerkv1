package fr.istic.mob.networkLY

import android.app.Application
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View

class Graph() : Application() {

        /**
         * contient la liste des figures, voir DessinView.java
         */
        //var listObject = mutableListOf<Figure?>()

        var listFigure = mutableListOf<Figure?>()

    init {
        listFigure = mutableListOf()
    }

        /**
         * retourne la liste de l'application
         * @return liste des figures
         */
        fun getFigures() : MutableList<Figure?> {
            return listFigure
        }
    }
