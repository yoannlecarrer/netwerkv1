package fr.istic.mob.networkLY

import android.graphics.*
import android.graphics.Color.BLACK
import android.util.Log

class Connection : Figure{

    override val paint = Paint()
    val path = Path()

    constructor(color: Int): super(){
        init()
    }

    fun init() {
        paint.style = Paint.Style.FILL
        paint.color = BLACK
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(15F);
        con = true
    }

    override fun draw(canvas: Canvas)
    {
        //rect= RectF(0F,0F,0F,0F)
        path.reset()
        path.moveTo(x1,y1)
        x2?.let { path.lineTo(it,y2) }
        canvas.drawPath(path,paint)
    }

}