package fr.istic.mob.networkLY

import android.graphics.*
import android.graphics.Color.BLUE
import android.graphics.Color.GREEN

class Object : Figure{

    override val paint = Paint()
    val paintName = Paint()

    constructor(color: Int) : super(){
        init()
    }

    fun init() {
        paint.style = Paint.Style.FILL_AND_STROKE
        paint.color = GREEN
        paintName.setColor(Color.WHITE)
        paintName.setTextAlign(Paint.Align.RIGHT)
        paintName.setTextSize(50F)
        paintName.setTypeface(Typeface.create("Arial", Typeface.BOLD))
        obj = true
    }

    override fun draw(canvas: Canvas)
    {
        rect = RectF(x1,y1,x1+110f,y1+110F)
        if(x2!=null) {
            rect = RectF(x2!!, y2, x2!! + 110f, y2 + 110F)
        }
        canvas.drawText(name,x1,y1+55,paintName)
        canvas.drawRect(rect,paint)
    }
}