package fr.istic.mob.networkLY

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF


abstract class Figure {

    var x1: Float = 0f
    var x2: Float? = null
    var y1: Float = 0f
    var y2: Float = 0f

    var rect = RectF()

    var name:String=""

    var obj = false
    var con = false

    var verrou = false

    // couleur tirée aléatoirement lors de sa création
    protected open val paint = Paint()

    enum class Type {
        // TODO ajouter les symboles LIGNE et ELLIPSE
        OBJECT, CONNECTION,NULL
    }

    companion object {
        fun creer(type: Type, color: Int): Figure? {
            when (type) {
                Type.OBJECT -> return Object(color)
                Type.CONNECTION -> return Connection(color)
                Type.NULL -> return null
            }
            return null
        }
    }

    /**
     * retourne la couleur de dessin de cette figure
     * @return Paint de la figure
     */
    fun getPaint()
    {
        paint
    }

    // point de départ
    open fun setReference(x: Float, y: Float) {
        x1 = x
        y1 = y
        rect = RectF(x1,y1,x1+110,y1+110)
    }

    open fun set_Name(n:String) {
        name=n
    }

    open fun verrouillage(b:Boolean){
        verrou = b
    }

    //vverouille l'object

    //point d'arriver
    open fun setCoin(x: Float, y: Float) {
        x2 = x
        y2 = y
    }


    /**
     * Cette méthode dessine la figure sur le canvas
     * Cette méthode est spécifique du type exact de la figure
     * @param canvas
     */
    abstract fun draw(canvas: Canvas)

}