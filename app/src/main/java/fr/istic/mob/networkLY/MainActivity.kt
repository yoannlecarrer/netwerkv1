package fr.istic.mob.networkLY

import android.os.Bundle
import android.util.Log
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.LinearLayout
import android.widget.Toast
import fr.istic.mob.networkLY.Figure.Type;
import kotlin.math.log

class MainActivity : AppCompatActivity() {

    lateinit var view:MyCanvasView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        view = MyCanvasView(this,null)
        setContentView(view)
        setContentView(R.layout.activity_main)
        val linearLayout = findViewById<LinearLayout>(R.id.layout)
        linearLayout.addView(view)
        //setSupportActionBar(findViewById(R.id.toolbar))
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            R.id.action_reset -> {
                view.clear()
                Toast.makeText(applicationContext, R.string.text_reset, Toast.LENGTH_LONG).show()
                return true
            }
            R.id.action_addObjects -> {
                view.setTypeFigure(Type.OBJECT)
                Toast.makeText(applicationContext, R.string.add_Objects, Toast.LENGTH_LONG).show()
                return true
            }
            R.id.action_addConnections -> {
                view.setTypeFigure(Type.CONNECTION)
                Toast.makeText(applicationContext, R.string.add_Connection, Toast.LENGTH_LONG).show()
                return true
            }
            R.id.action_editConnectionsObjects -> {
                view.setTypeFigure((Type.NULL))
                Toast.makeText(applicationContext, R.string.edit_Connection, Toast.LENGTH_LONG).show()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}