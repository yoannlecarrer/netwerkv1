package fr.istic.mob.networkLY

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Canvas
import android.graphics.Color.BLUE
import android.text.InputType
import android.util.AttributeSet
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.widget.EditText
import fr.istic.mob.networkLY.Figure.Type;

class MyCanvasView : View {

    private val color = BLUE
    private var figures = mutableListOf<Figure?>()
    private var typefigure = Type.NULL

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        var app: Graph = context.applicationContext as Graph
        figures = app.getFigures()

    }

    override fun onDraw(canvas: Canvas) {
        for (figure in figures) {
            if (figure != null) {
                figure.draw(canvas)
            }
        }
    }

    /**
     * Clique de l'écran
     */
    override fun onTouchEvent(event: MotionEvent): Boolean {
        val x = event.x
        val y = event.y

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                if(typefigure==Type.CONNECTION) {
                    actionDownConnection(x, y)
                }else if(typefigure==Type.OBJECT){
                    gestureDetector.onTouchEvent(event)
                }else if(typefigure==Type.NULL){
                    actionDownNull(x, y)
                }
            }
            MotionEvent.ACTION_MOVE -> {
                if(typefigure == Type.CONNECTION) {
                    actionMoveConnection(x, y)
                }else if (typefigure==Type.NULL){
                    actionMoveNull(x, y)
                }
            }
            MotionEvent.ACTION_UP ->{
                if(typefigure == Type.CONNECTION) {
                    actionUpConnection(x, y)
                }else if(typefigure==Type.NULL){
                    actionUpNull()
                }
            }
        }
        invalidate()
        return true
    }

    /**
      * Event modifer un OBJECT
      */
    private fun actionDownNull(x: Float, y: Float) {
        for (i in figures) {
            if (i != null) {
                if( i.rect.contains(x,y)){
                    i.verrouillage(true)
                    return
                }
            }
        }
    }

    private fun actionMoveNull(x: Float, y: Float) {
        for (i in figures) {
            if (i != null) {
                if(i.verrou && i.obj){
                    val xCenter = i.rect.centerX()
                    val yCenter = i.rect.centerY()
                    i.setReference(x,y)
                    for (j in figures){
                        if (j != null && j.con) {
                            if (j.x1 == xCenter && j.y1 == yCenter) {
                                j.setReference(i.rect.centerX(),i.rect.centerY())
                            }else if(j.x2 == xCenter && j.y2 == yCenter){
                                j.setCoin(i.rect.centerX(),i.rect.centerY())
                            }
                        }
                    }
                    return
                }
            }
        }
    }

    private fun actionUpNull() {
        for (i in figures) {
            if (i != null) {
                if(i.verrou){
                    println(i)
                    i.verrouillage(false)
                    return
                }
            }
        }
    }

    /**
     * Event CONNECTION
     */
    private fun actionDownConnection(x: Float, y: Float){
        if(figures.isEmpty()){
            return
        }else {
            for (i in figures) {
                if (i != null) {
                    if (i.rect.contains(x, y)) {
                        val figure = Figure.creer(typefigure, color)
                        if (figure != null) {
                            figure.setReference(i.rect.centerX(), i.rect.centerY())
                            figure.verrouillage(true)
                            figures.add(figure)
                            return
                        }
                    }
                }
            }
            return
        }
    }

    private fun actionMoveConnection(x: Float, y: Float) {
        for (i in figures) {
            if (i != null) {
                if (i.verrou) {
                    i.setCoin(x,y)
                    return
                }
            }
        }
    }

    private fun actionUpConnection(x: Float, y: Float) {
        if(figures.isEmpty()){
            return
        }else {
            for (i in figures) {
                if (i != null) {
                    if (i.rect.contains(x, y)) {
                        figures.last()?.setCoin(i.rect.centerX(), i.rect.centerY())
                        figures.last()?.verrouillage(false)
                        return
                    }
                }
            }
            if (figures.last()?.verrou!! == true) {
                figures.removeLast()
                return
            }
        }
    }

    /**
     *Long press pour créer un OBJECT
     */
    val gestureDetector = GestureDetector(object:GestureDetector.SimpleOnGestureListener() {
        override fun onLongPress(event:MotionEvent) {
            var xCoor = event.x
            var yCoor = event.y
            when (event.getAction()) {
                MotionEvent.ACTION_DOWN -> {
                    val builder = AlertDialog.Builder(getContext())
                    builder.setTitle("Enter object name")
                    val input = EditText(getContext())
                    input.setInputType(InputType.TYPE_CLASS_TEXT)
                    builder.setView(input)
                    builder.setPositiveButton("OK", object: DialogInterface.OnClickListener {
                        override fun onClick(dialog:DialogInterface, which:Int) {
                            val rectangleName = input.getText().toString()
                            typefigure = Type.OBJECT
                            val figure = Figure.creer(typefigure, color)
                            if (figure != null) {
                                figure.setReference(xCoor, yCoor)
                                figure.set_Name(rectangleName)
                                figures.add(figure)
                            }
                        }
                    })
                    builder.setNegativeButton("Cancel", object: DialogInterface.OnClickListener {
                        override fun onClick(dialog:DialogInterface, which:Int) {
                            dialog.cancel()
                            typefigure = Type.OBJECT
                        }
                    })
                    builder.show()
                    typefigure = Type.NULL
                    invalidate()
                }
            }
        }
    })

    /**
     * définit le type de la prochaine figure à dessiner
     * @param type
     */
    fun setTypeFigure(type: Type) {
        typefigure = type
        Log.i("type",typefigure.toString())
    }

    /**
     * supprime tout les figure et rénitialise le Canvas
     * @param type
     */
    fun clear() {
        figures.clear()
        invalidate()
    }


}
