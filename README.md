# KOTLIN

Construire une application gérant les objets connectés dans un appartement
Le menu principal propose :
- Réinitialiser le réseau,
- mode pour l’ajout d’objets,
- mode pour l’ajout de connexions,
- mode pour la modification d’objets/connexions.
- L’application contient une zone de création d’un réseau qui occupe tout l’écran sauf la barre demenu. La mise en page peut être la même en mode portrait/paysage (mais le réseau créé ne doit pas disparaître à la rotation).
- La classe Graph est le modèle du réseau. Il contient la description du graphe des connexions :
- objets, connexions, couleurs, positions, etc.
- À l’ouverture, un plan d’appartement est affiché, sans aucun objet connecté.
- Il est possible d’ajouter des objets, avec un long–click. L’étiquette de l’objet doit être précisée pendant la création. Elle est affichée à côté de l’objet.
- Quand on pose le doigt sur un objet et on déplace ce doigt, le objet se déplace avec le doigt (et avec les connexions bien sûr).
- Il est possible d’ajouter des connexions, en posant le doigt sur un objet et en le suivant jusqu’à un autre objet (la connexion temporaire est visible et est modifiée quand on déplace le doigt). La connexion n’est pas créée si le doigt est lâché ailleurs que sur un objet. La connexion est créée en ligne droite.
- L’application doit être entièrement bilingue anglais/français. Le français est la langue par défaut.
- On ne s’occupe pas du chevauchement entre les objets et les connexions. L’utilisateur doit les organiser lui–même de façon lisible.
